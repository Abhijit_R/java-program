public class LinkedListImplementations {
    public static class Node    {
        int data;
        Node next;
        Node(int data){
            this.data = data;
        }
    }
    public static class linkedList{
        Node head = null;
        Node tail = null;
        int size = 0;

        void insertAtEnd(int val){
            Node temp = new Node(val);
            if(head == null){           // empty list
                head = temp;
            }else{                      // non-empty list
                tail.next = temp;
            }
            tail = temp;
            size++;
        }
        void insertAtStart(int val){
            Node temp = new Node(val);
            if(head == null){
                head = temp;
                tail = temp;
            }else{
                temp.next = head;
                head = temp;
            }
            size++;
        }
        void display(){
            Node temp = head;
            while(temp != null){
                System.out.print(temp.data+" ");
                temp = temp.next;
            }
            System.out.println(" ");
        }
        int size(){
            int count = 0;
            Node temp = head;
            while(temp != null){
                count++;
                System.out.println(temp.data);
                temp = temp.next;
            }
            return count;
        }
        void insertAt(int idx,int val){
            Node t = new Node(val);
            Node temp = head;
            if(idx==size()){
                insertAtEnd(val);
                return;
            }
            if(idx == 0){
                insertAtStart(val);
                return;
            }
            for (int i = 0;i<=idx-1;i++){
                temp = temp.next;
            }
            t.next = temp.next;
            temp.next = t;
            size++;
        }

        int getElementAt(int idx){
            Node temp = head;
            for (int i = 1;i<=idx-1;i++){
                temp = temp.next;
            }
            return temp.data;
        }

        void deleteAt(int idx){
            Node temp = head;
            if(idx == 0){
                head=head.next;
                size--;
                return;
            }
            for (int i = 1; i <= idx-1 ; i++) {
                temp =temp.next;
            }
            temp.next =temp.next.next;
            if (temp.next == null) {
                tail = temp;
            }
            size--;
        }

    }

    public static void main(String[] args) {
        linkedList ll = new linkedList();
        ll.insertAtEnd(1);
        ll.insertAtEnd(3);
//        ll.display();
//        System.out.println("linked list size :" + ll.size());
//        System.out.println("insert at start");
        ll.insertAtStart(0);
//        ll.display();
//        System.out.println("insert at index");
        ll.insertAt(1,10);
//        ll.display();
//        System.out.println("element at index");
//        System.out.println(ll.getElementAt(2));
//        System.out.println(ll.size);

        ll.display();
        ll.deleteAt(1);
        ll.display();
        System.out.println(ll.tail.data);
        System.out.println(ll.head.data);
    }
}
