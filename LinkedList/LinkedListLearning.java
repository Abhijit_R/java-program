public class LinkedListLearning {
    public static class Node{
        int data; // Value
        Node next; // address of the next
        Node(int data){
            this.data=data;
        }
    }

    public static void displayLinkedListRecursively(Node head){
        if (head == null) return;
        System.out.println(head.data);
        displayLinkedListRecursively(head.next);
    }

    public static void displayLinkedListRecursivelyReverse(Node head){
        if (head == null) return;
        displayLinkedListRecursivelyReverse(head.next);
        System.out.println(head.data);
    }

    public static void displayLinkedList(Node head){
        Node temp = head;
        while (temp!=null){
            System.out.println(temp.data);
            temp = temp.next;
        }
    }

    public static int lengthLinkedList(Node head){
        Node temp = head;
        int count = 0;
        while (temp!=null){
            count++;
            System.out.println(temp.data);
            temp = temp.next;
        }
        return count;
    }
    public static void main(String[] args) {
        Node a = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);

//        System.out.println(x);

        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;

        System.out.println("looping");
        displayLinkedList(a);
        System.out.println("recursion");
        displayLinkedListRecursively(a);
        System.out.println("reverse recursion");
        displayLinkedListRecursivelyReverse(a);

        System.out.println("length of linkedlist looping "+lengthLinkedList(a));

        System.out.println(a.next.data);
        System.out.println(b.data);
        Node temp = a;
//        for (int i =1;i<=5;i++){
//            System.out.println(temp.data);
//            temp = temp.next;
//        }
//        while (temp!=null){
//            System.out.println(temp.data);
//            temp = temp.next;
//        }
    }
}
