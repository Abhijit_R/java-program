class LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }

        String prefix = strs[0];

        for (int i = 1; i < strs.length; i++) {
            // Adjust the prefix to match the current string
            while (strs[i].indexOf(prefix) != 0) {
                // Shorten the prefix by one character at a time
                prefix = prefix.substring(0, prefix.length() - 1);
                // If the prefix is reduced to an empty string, return an empty string
                if (prefix.isEmpty()) {
                    return "";
                }
            }
        }

        // Return the longest common prefix found
        return prefix;
    }

    public static void main(String[] args) {
        Solution sol = new Solution();
        
        String[] example1 = {"flower", "flow", "flight"};
        System.out.println("Example 1: " + sol.longestCommonPrefix(example1)); // Output: "fl"
        
        String[] example2 = {"dog", "racecar", "car"};
        System.out.println("Example 2: " + sol.longestCommonPrefix(example2)); // Output: ""
    }
}
